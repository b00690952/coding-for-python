import unittest
from unittest.mock import patch, Mock

from mysql.connector import InterfaceError

from src.services.scores_service import ScoresService


class TestScoresService(unittest.TestCase):

    def setUp(self):
        self.scores_service = ScoresService('user', 'pass', 'host', 'database')

    def test_signature(self):
        self.assertIsInstance(self.scores_service, ScoresService)

    @patch('src.services.scores_service.mysql.connector')
    def test_get_scores(self, mocked_connector):
        rows = [(1, 2), (3, 4)]
        expected_result = [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]
        description = (('A', None, None, None, None, None, None), ('B', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchall.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.get_scores()

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_get_scores_handles_exception(self, mocked_connector):
        expected_result = []
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.get_scores()

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_get_scores_handles_invalid_credentials(self):
        expected_result = []
        expected_success = False
        success, result = self.scores_service.get_scores()

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_add_score(self, mocked_connector):
        rows = (3, 2, 1, 91, 3)
        request_data = {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91}
        expected_result = {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91, "score_id": 3}
        description = (
            ('away_score', None, None, None, None, None, None), ('home_score', None, None, None, None, None, None),
            ('match_id', None, None, None, None, None, None), ('match_length', None, None, None, None, None, None),
            ('score_id', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchone.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.add_score(request_data)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_add_score_handles_bad_json_input(self, mocked_connector):
        expected_result = {}
        expected_success = False

        cursor = Mock()
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.add_score(
            {"away_score": 3, "home_score": "invalid", "match_id": 1, "match_length": 91})

        assert not mocked_connector.connect.called
        assert not connect.cursor.called
        assert not cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_add_score_handles_exception(self, mocked_connector):
        expected_result = {}
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.add_score(
            {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91})

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_add_score_handles_invalid_credentials(self):
        expected_result = {}
        expected_success = False
        success, result = self.scores_service.add_score({})

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_update_score(self, mocked_connector):
        rows = (3, 2, 1, 91, 3)
        request_data = {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91}
        expected_result = {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91, "score_id": 3}
        description = (
            ('away_score', None, None, None, None, None, None), ('home_score', None, None, None, None, None, None),
            ('match_id', None, None, None, None, None, None), ('match_length', None, None, None, None, None, None),
            ('score_id', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchone.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.update_score(request_data, 3)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_update_score_handles_bad_json_input(self, mocked_connector):
        expected_result = {}
        expected_success = False

        cursor = Mock()
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.update_score(
            {"away_score": 3, "home_score": "invalid", "match_id": 1, "match_length": "91"}, 1)

        assert not mocked_connector.connect.called
        assert not connect.cursor.called
        assert not cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_update_score_handles_invalid_id(self, mocked_connector):
        expected_result = {}
        expected_success = False
        description = (
            ('away_score', None, None, None, None, None, None), ('home_score', None, None, None, None, None, None),
            ('match_id', None, None, None, None, None, None), ('match_length', None, None, None, None, None, None),
            ('score_id', None, None, None, None, None, None))

        cursor = Mock()
        cursor.fetchone.return_value = None
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.update_score(
            {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91}, 0)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_update_score_handles_exception(self, mocked_connector):
        expected_result = {}
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.update_score(
            {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91}, 0)

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_update_score_handles_invalid_credentials(self):
        expected_result = {}
        expected_success = False
        success, result = self.scores_service.update_score({}, 0)

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_delete_score(self, mocked_connector):
        rows = (3, 2, 1, 91, 3)
        description = (
            ('away_score', None, None, None, None, None, None), ('home_score', None, None, None, None, None, None),
            ('match_id', None, None, None, None, None, None), ('match_length', None, None, None, None, None, None),
            ('score_id', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchone.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success = self.scores_service.delete_score(3)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)

    @patch('src.services.scores_service.mysql.connector')
    def test_delete_score_handles_invalid_id(self, mocked_connector):
        expected_success = False
        description = (
            ('away_score', None, None, None, None, None, None), ('home_score', None, None, None, None, None, None),
            ('match_id', None, None, None, None, None, None), ('match_length', None, None, None, None, None, None),
            ('score_id', None, None, None, None, None, None))

        cursor = Mock()
        cursor.fetchone.return_value = None
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success = self.scores_service.delete_score(0)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert not connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)

    @patch('src.services.scores_service.mysql.connector')
    def test_delete_score_handles_exception(self, mocked_connector):
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success = self.scores_service.delete_score(0)

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)

    def test_delete_score_handles_invalid_credentials(self):
        expected_success = False
        success = self.scores_service.delete_score(0)

        self.assertEqual(expected_success, success)

    @patch('src.services.scores_service.mysql.connector')
    def test_get_matches_without_scores_scores(self, mocked_connector):
        rows = [(1,), (3,)]
        expected_result = [{'A': 1}, {'A': 3}]
        description = (('A', None, None, None, None, None, None),)
        expected_success = True

        cursor = Mock()
        cursor.fetchall.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.get_matches_without_scores()

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.scores_service.mysql.connector')
    def test_get_matches_without_scores_handles_exception(self, mocked_connector):
        expected_result = []
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.scores_service.get_matches_without_scores()

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_get_matches_without_scores_handles_invalid_credentials(self):
        expected_result = []
        expected_success = False
        success, result = self.scores_service.get_matches_without_scores()

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def tearDown(self):
        self.scores_service = None
