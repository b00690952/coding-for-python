# Mangers Table
DROP TABLE IF EXISTS `managers`;
CREATE TABLE `managers` (
  `manager_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`manager_id`)
);

INSERT INTO `managers` VALUES (1,'Guardole Pep',47,'Spanish'),
                              (2,'Fonseca Paulo',45,'portuguese'),
                              (3,'Ouzounidis Marinos',50,'Greek'),
                              (4,'Ten Hag Erik',48,'Dutch'),
                              (5,'Sukiasyan Varuzhan',47,'Armenian'),
                              (6,'Baltazar Bruno',41,'Portuguese'),
                              (7,'Di Francesco Eusebio',49,'Italian'),
                              (8,'Simeone Diego',48,'Argentinian');

# Referees Table
DROP TABLE IF EXISTS `referees`;
CREATE TABLE `referees` (
  `referee_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`referee_id`)
);

INSERT INTO `referees` VALUES (1,'Viktor Kassai',43,'Hungarian'),
                              (2, 'Milorad Mažić', 45, 'Serbian'),
                              (3, 'Björn Kuipers', 45, 'Dutch'),
                              (4, 'Felix Brych', 43, 'German'),
                              (5, 'Damir Skomina', 42, 'Slovenian');

# Teams Table
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`team_id`),
  KEY `manager_id_idx` (`manager_id`),
  CONSTRAINT `team_manager_fk` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`manager_id`)
);

INSERT INTO `teams` VALUES (1,1,'Manchester City','English'),
                           (2,2,'Shakhtar Donetsk','Ukrainian'),
                           (3,3,'AEK Athens FC','Greek'),
                           (4,4,'Ajax','Dutch'),
                           (5,5,'Alashkert','Armenian'),
                           (6,6,'APOEL','Cyprian'),
                           (7,7,'AS Roma','Italian'),
                           (8,8,'Atl. Madrid','Spanish');

# Stadiums Table
DROP TABLE IF EXISTS `stadiums`;
CREATE TABLE `stadiums` (
  `stadium_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`stadium_id`),
  KEY `team_id_idx` (`team_id`),
  CONSTRAINT `stadium_team_fk` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`)
);

INSERT INTO `stadiums` VALUES (1,1,55097,'Manchester','Etihad Stadium'),
                              (2,2,40003,'Kharkiv','Metalist Oblast Sports Complex'),
                              (3,3,69618,'Athens','Athens Olympic Stadium'),
                              (4,4,54990,'Amsterdam','Johan Cruijff Arena'),
                              (5,5,6850,'Yerevan','Alashkert Stadium'),
                              (6,6,22859,'Strovolos','GSP Stadium'),
                              (7,7,72698,'Roma','Stadio Olimpico'),
                              (8,8,68000,'Madrid','Wanda Metropolitano');

# Players Table
DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `player_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`player_id`),
  KEY `team_id_idx` (`team_id`),
  CONSTRAINT `player_team_fk` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`)
);

INSERT INTO `players` VALUES (1,1,'Kompany Vincent',32,'German','Defender'),
                             (2,1,'Foden Phil',18,'English','Midfielder'),
                             (3,2,'Pyatov Andrij',34,'Ukranian','Goalkeeper'),
                             (4,2,'Cipriano Marcos',19,'Brazilian','Forward'),
                             (5,3,'Barkas Vasilios',24,'Greek','Goalkeeper'),
                             (6,3,'Albanis Christos',24,'Greek','Forward'),
                             (7,4,'Bakboord Navajo',19,'Dutch','Defender'),
                             (8,4,'Ylatupa Saku',19,'Finish','Midfielder'),
                             (9,5,'Grigoryan Artak',31,'Armenian','Forward'),
                             (10,5,'Cancarevic Ognjen',29,'Serbian','Goalkeeper'),
                             (11,6,'Carlao Carlao',32,'Brazilian','Defender'),
                             (12,6,'Vouros Praxitelis',23,'Greek','Defender'),
                             (13,7,'Bianda William',18,'French','Defender'),
                             (14,7,'Bucri Flavio',17,'Italian','Forward'),
                             (15,8,'Riquelme Rodrigo',18,'Spanish','Defender'),
                             (16,8,'Moya Antonio',20,'Spanish','Midfielder');

# Matches Table
DROP TABLE IF EXISTS `matches`;
CREATE TABLE `matches` (
  `match_id` int(11) NOT NULL AUTO_INCREMENT,
  `home_team_id` int(11) NOT NULL,
  `away_team_id` int(11) NOT NULL,
  `stadium_id` int(11) NOT NULL,
  `referee_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`match_id`),
  KEY `match_home_team_fk_idx` (`home_team_id`),
  KEY `match_away_team_fk_idx` (`away_team_id`),
  KEY `match_stadium_fk_idx` (`stadium_id`),
  KEY `match_referee_fk_idx` (`referee_id`),
  CONSTRAINT `match_away_team_fk` FOREIGN KEY (`away_team_id`) REFERENCES `teams` (`team_id`),
  CONSTRAINT `match_home_team_fk` FOREIGN KEY (`home_team_id`) REFERENCES `teams` (`team_id`),
  CONSTRAINT `match_referee_fk` FOREIGN KEY (`referee_id`) REFERENCES `referees` (`referee_id`),
  CONSTRAINT `match_stadium_fk` FOREIGN KEY (`stadium_id`) REFERENCES `stadiums` (`stadium_id`)
);

INSERT INTO `matches` VALUES (1,1,2,1,1,'2018-11-07 17:00:00'),
                             (2,2,3,2,2,'2018-11-08 18:30:00'),
                             (3,3,5,3,3,'2018-11-09 19:00:00'),
                             (4,4,1,4,4,'2018-11-10 17:30:00'),
                             (5,5,8,5,5,'2018-11-11 20:15:00'),
                             (6,6,7,6,1,'2018-11-12 21:00:00'),
                             (7,7,2,7,2,'2018-11-13 18:46:00');

# Scores Table
DROP TABLE IF EXISTS `scores`;
CREATE TABLE `scores` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `match_id` int(11) NOT NULL,
  `home_score` int(11) DEFAULT NULL,
  `away_score` int(11) DEFAULT NULL,
  `match_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`score_id`),
  KEY `score_match_fk_idx` (`match_id`),
  CONSTRAINT `score_match_fk` FOREIGN KEY (`match_id`) REFERENCES `matches` (`match_id`)
);

INSERT INTO `scores` VALUES (1,1,6,0,93),
                            (2,2,1,2,91),
                            (3,3,2,1,92),
                            (4,4,1,1,91),
                            (5,5,2,4,90);
