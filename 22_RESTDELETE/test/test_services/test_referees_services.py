import unittest
from unittest.mock import patch, Mock

from mysql.connector import InterfaceError

from src.services.referees_service import RefereesService


class TestRefereesService(unittest.TestCase):

    def setUp(self):
        self.referees_service = RefereesService('user', 'pass', 'host', 'database')

    def test_signature(self):
        self.assertIsInstance(self.referees_service, RefereesService)

    @patch('src.services.referees_service.mysql.connector')
    def test_get_referees(self, mocked_connector):
        rows = [(1, 2), (3, 4)]
        expected_result = [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]
        description = (('A', None, None, None, None, None, None), ('B', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchall.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.get_referees()

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_get_referees_handles_exception(self, mocked_connector):
        expected_result = []
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.get_referees()

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_get_referees_handles_invalid_credentials(self):
        expected_result = []
        expected_success = False
        success, result = self.referees_service.get_referees()

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_add_referees(self, mocked_connector):
        rows = ("Harry Potter", 52, "British", 3)
        request_data = {"name": "Harry Potter", "age": 52, "nationality": "British"}
        expected_result = {"name": "Harry Potter", "age": 52, "nationality": "British", "referee_id": 3}
        description = (
            ('name', None, None, None, None, None, None), ('age', None, None, None, None, None, None),
            ('nationality', None, None, None, None, None, None),
            ('referee_id', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchone.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.add_referees(request_data)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_add_referees_handles_bad_json_input(self, mocked_connector):
        expected_result = {}
        expected_success = False

        cursor = Mock()
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.add_referees(
            {"name": 5, "age": 52, "nationality": "British"})

        assert not mocked_connector.connect.called
        assert not connect.cursor.called
        assert not cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_add_referees_handles_exception(self, mocked_connector):
        expected_result = {}
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.add_referees(
            {"name": 'Harry Potter', "age": 52, "nationality": 'British'})

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_add_referees_handles_invalid_credentials(self):
        expected_result = {}
        expected_success = False
        success, result = self.referees_service.add_referees({})

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_update_referee(self, mocked_connector):
        rows = ("Harry Potter", 52, "British", 3)
        request_data = {"name": "Harry Potter", "age": 52, "nationality": "British"}
        expected_result = {"name": "Harry Potter", "age": 52, "nationality": "British", "referee_id": 3}
        description = (
            ('name', None, None, None, None, None, None), ('age', None, None, None, None, None, None),
            ('nationality', None, None, None, None, None, None),
            ('referee_id', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchone.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.update_referee(request_data, 3)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_update_referee_handles_bad_json_input(self, mocked_connector):
        expected_result = {}
        expected_success = False

        cursor = Mock()
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.update_referee(
            {"name": 5, "age": 52, "nationality": "British"}, 3)

        assert not mocked_connector.connect.called
        assert not connect.cursor.called
        assert not cursor.execute.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_update_referee_handles_invalid_id(self, mocked_connector):
        expected_result = {}
        expected_success = False
        description = (
            ('name', None, None, None, None, None, None), ('age', None, None, None, None, None, None),
            ('nationality', None, None, None, None, None, None),
            ('referee_id', None, None, None, None, None, None))

        cursor = Mock()
        cursor.fetchone.return_value = None
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.update_referee(
            {"name": "Harry Potter", "age": 52, "nationality": "British"}, 0)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_update_referee_handles_exception(self, mocked_connector):
        expected_result = {}
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.update_referee(
            {"name": "Harry Potter", "age": 52, "nationality": "British"}, 0)

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_update_referee_handles_invalid_credentials(self):
        expected_result = {}
        expected_success = False
        success, result = self.referees_service.update_referee({}, 0)

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_get_referee_handles_exception(self, mocked_connector):
        expected_result = ""
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success, result = self.referees_service.get_referee(1)

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    def test_get_referee_handles_invalid_credentials(self):
        expected_result = ""
        expected_success = False
        success, result = self.referees_service.get_referee(0)

        self.assertEqual(expected_success, success)
        self.assertEqual(expected_result, result)

    @patch('src.services.referees_service.mysql.connector')
    def test_delete_referee(self, mocked_connector):
        rows = ("Harry Potter", 52, "British", 3)
        description = (
            ('name', None, None, None, None, None, None), ('age', None, None, None, None, None, None),
            ('nationality', None, None, None, None, None, None),
            ('referee_id', None, None, None, None, None, None))
        expected_success = True

        cursor = Mock()
        cursor.fetchone.return_value = rows
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success = self.referees_service.delete_referee(3)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert cursor.execute.called
        assert connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)

    @patch('src.services.referees_service.mysql.connector')
    def test_delete_referee_handles_invalid_id(self, mocked_connector):
        expected_success = False
        description = (
            ('name', None, None, None, None, None, None), ('age', None, None, None, None, None, None),
            ('nationality', None, None, None, None, None, None),
            ('referee_id', None, None, None, None, None, None))

        cursor = Mock()
        cursor.fetchone.return_value = None
        cursor.description = description
        connect = Mock()
        connect.cursor.return_value = cursor
        mocked_connector.connect.return_value = connect

        success = self.referees_service.delete_referee(0)

        assert mocked_connector.connect.called
        assert connect.cursor.called
        assert not connect.commit.called
        assert cursor.fetch_one_called

        self.assertEqual(expected_success, success)

    @patch('src.services.referees_service.mysql.connector')
    def test_delete_referee_handles_exception(self, mocked_connector):
        expected_success = False

        connect = Mock()
        connect.cursor.side_effect = InterfaceError
        mocked_connector.connect.return_value = connect

        success = self.referees_service.delete_referee(0)

        assert mocked_connector.connect.called
        assert connect.cursor.called

        self.assertEqual(expected_success, success)

    def test_delete_referee_handles_invalid_credentials(self):
        expected_success = False
        success = self.referees_service.delete_referee(0)

        self.assertEqual(expected_success, success)

    def tearDown(self):
        self.referees_service = None
