import mysql.connector


class RefereesService:
    __GET_REFEREES_SQL = "SELECT * FROM referees"
    __GET_REFEREE_SQL = "SELECT * FROM referees WHERE referee_id ="
    __ADD_REFEREES_SQL = (
        "INSERT INTO referees(name, age, nationality) "
        "VALUES(%s, %s, %s, %s)"
    )
    __UPDATE_REFEREE_SQL = (
        "UPDATE referees SET name = %s, age = %s, nationality = %s WHERE referee_id="
    )
    __DELETE_REFEREE_SQL = (
        "DELETE FROM referee WHERE referee_id="
    )

    def __init__(self, user, password, host, database):
        self.__user = user
        self.__password = password
        self.__host = host
        self.__database = database

    def get_referees(self):
        """
        Queries the database for all referees from the referees table and returns all of the
        rows in the table and relevant data from foreign keys including:

        :return: Success flag, All rows and related information from the referees table
        :rtype: (bool, list of dict)
        """
        result = []
        success = False

        try:
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__GET_REFEREES_SQL)
            rows = cursor.fetchall()
            columns = cursor.description
            connection.close()
            for row in rows:
                result.append(self.__build_result(columns, row))
            success = True
        except Exception as ex:
            print('Something went wrong when getting referees: {}'.format(ex))

        return success, result

    def add_referees(self, json):
        """
        Adds new referee to the database, and returns it if successful

        :param json: json object from post request
        :type json: dict
        :return: Success flag, New row added to referees table
        :rtype: (bool, dict)
        """
        result = {}
        success = False

        try:
            if not self.__validate_input_json(json):
                raise TypeError('Input data not valid')
            # add new row
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__ADD_REFEREES_SQL, (
                json['name'],
                json['age'],
                json['nationality']
            ))
            new_id = cursor.lastrowid
            connection.commit()

            # get new row
            row, columns = self.__get_referee(cursor, new_id)
            connection.close()

            result = self.__build_result(columns, row)
            success = True
        except Exception as ex:
            print('Something went wrong when adding referee: {}'.format(ex))

        return success, result

    def update_referee(self, json, referee_id):
        """
        Update referee from the database, and returns if it was successful

        :param json: json object from post request
        :type json: dict
        :param referee_id: id of the referee to be updated
        :type referee_id: int
        :return: Success flag, New row added to referees table
        :rtype: (bool, dict)
        """
        result = {}
        success = False

        try:
            if not self.__validate_input_json(json):
                raise TypeError('Input data not valid')
            # add new row
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__UPDATE_REFEREE_SQL + str(referee_id), (
                json['name'],
                json['age'],
                json['nationality'],
            ))
            connection.commit()

            # get updated row
            row, columns = self.__get_referee(cursor, referee_id)
            connection.close()
            if row is None:
                raise TypeError('Update performed on invalid referee_id, row doesn\'t exist')

            result = self.__build_result(columns, row)
            success = True
        except Exception as ex:
            print('Something went wrong when updating referee: {}'.format(ex))

        return success, result

    def get_referee(self, id):
        """
        Queries the database for an individual referee in the referees table and returns the row in the table,
        and relevant data

        """
        data = ""
        try:
            connection = self.__get_connection()
            cursor = connection.cursor()
            sql = self.__GET_REFEREE_SQL + str(id)
            cursor.execute(sql)
            row = cursor.fetchone()
            columns = cursor.description
            data = self.__build_result(columns, row)
            success = True
        except Exception as ex:
            print('Something has went wrong when getting referee {}'.format(ex))
            success = False

        return success, data

    def delete_referee(self, referee_id):
        """
        Update referee from the database, and returns if it was successful

        :param referee_id: id of the referee to be deleted
        :type referee_id: int
        :return: Success flag
        :rtype: bool
        """
        success = False

        try:
            # Check if referee exists
            connection = self.__get_connection()
            cursor = connection.cursor()
            row, columns = self.__get_referee(cursor, referee_id)
            if row is None:
                raise TypeError('Delete performed on invalid referee_id, row doesn\'t exist')

            # Delete score
            cursor.execute(self.__DELETE_REFEREE_SQL + str(referee_id))
            connection.commit()
            connection.close()
            success = True
        except Exception as ex:
            print('Something went wrong when deleting referee: {}'.format(ex))

        return success

    def __get_connection(self):
        # Returns a connection to the database
        return mysql.connector.connect(
            user=self.__user,
            password=self.__password,
            host=self.__host,
            database=self.__database)

    def __get_referee(self, cursor, referee_id):
        # Gets an individual row, and returns the row and columns
        sql = self.__GET_REFEREE_SQL + str(referee_id)
        cursor.execute(sql)
        row = cursor.fetchone()
        columns = cursor.description
        return row, columns

    def __validate_input_json(self, json):
        if (isinstance(json['name'], str)
                and isinstance(json['age'], int)
                and isinstance(json['nationality'], str)):
            return True
        else:
            return False

    def __build_result(self, columns, row):
        # Creates dictionary from columns and rows
        row_dict = {}
        for (key, value) in zip(columns, row):
            row_dict[key[0]] = value
        return row_dict
