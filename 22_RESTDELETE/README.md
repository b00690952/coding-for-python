# Team 29 #

For information about git flow, db schema etc. see the [wiki](https://bitbucket.org/tarr-c/com661_1819_team_29/wiki/Home)

## Setting up environment
Install python 3.7.1 from [here](https://www.python.org/downloads/)  
Clone the repository  
Open the project in PyCharm  
In the terminal run _pip install -r requirements_

## Running API
To start the API run _api.py_  
To test that it is active navigate to [http://127.0.0.1:5000/](http://127.0.0.1:5000/) and you should see **Team 29 API**  
Update USER, PASSWORD, HOST, DATABASE in api.py to your database credentials

## Setting up DB
To set up the database import _team_29_db_dump.sql_ into MySQL Workbench follow the guide at the bottom of 
this [page](https://dev.mysql.com/doc/workbench/en/wb-admin-export-import-management.html)
