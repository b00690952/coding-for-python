import unittest
from unittest.mock import patch

from werkzeug.exceptions import HTTPException

import src.api as api


class TestAPI(unittest.TestCase):

    def test_index_response(self):
        test_api = api.app.test_client()
        response = test_api.get('/')
        self.assertEqual('<Response streamed [200 OK]>', str(response))

    def test_home_panel_response(self):
        test_api = api.app.test_client()
        response = test_api.get('/homePanel.html')
        self.assertEqual('<Response streamed [200 OK]>', str(response))

    def test_scores_panel_response(self):
        test_api = api.app.test_client()
        response = test_api.get('/scoresPanel.html')
        self.assertEqual('<Response streamed [200 OK]>', str(response))

    def test_base_uri_response(self):
        test_api = api.app.test_client()
        response = test_api.get('/api/v1')
        self.assertEqual('<Response streamed [200 OK]>', str(response))

    def test_invalid_uri_response(self):
        test_api = api.app.test_client()
        response = test_api.get('/not/a/valid/uri')
        self.assertEqual('<Response streamed [404 NOT FOUND]>', str(response))

    def test_api_base_uri(self):
        self.assertEqual('Team 29 API', api.api_base_uri())

    @patch('src.api.jsonify')
    @patch('src.services.scores_service.ScoresService.get_scores')
    def test_get_scores_successful(self, get_scores, json_return_value):
        expected_json = {'scores': [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]}
        get_scores.return_value = True, [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]
        json_return_value.return_value = expected_json
        returned_value, response = api.get_scores()

        assert get_scores.called
        assert json_return_value.called
        self.assertEqual(expected_json, returned_value)
        self.assertEqual(200, response)

    @patch('src.services.scores_service.ScoresService.get_scores')
    def test_get_scores_unsuccessful(self, get_scores):
        get_scores.return_value = False, []
        returned_value, response = api.get_scores()

        assert get_scores.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.jsonify')
    @patch('src.services.scores_service.ScoresService.add_score')
    @patch('src.api.request')
    def test_add_score_valid_request_successful(self, mocked_request, add_score, json_return_value):
        mocked_request_data = {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91}
        scores_service_return = (
            True, {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91, "score_id": 3})
        mocked_request.json.return_value = mocked_request_data
        add_score.return_value = scores_service_return
        json_return_value.return_value = scores_service_return[1]
        returned_value, response = api.add_score()

        assert add_score.called
        assert json_return_value.called
        self.assertEqual(scores_service_return[1], returned_value)
        self.assertEqual(201, response)

    @patch('src.services.scores_service.ScoresService.add_score')
    @patch('src.api.request')
    def test_add_score_valid_request_unsuccessful(self, mocked_request, add_score):
        mocked_request_data = {}
        scores_service_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        add_score.return_value = scores_service_return
        returned_value, response = api.add_score()

        assert add_score.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_add_score_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.add_score()

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.api.jsonify')
    @patch('src.services.scores_service.ScoresService.update_score')
    @patch('src.api.request')
    def test_update_score_valid_request_successful(self, mocked_request, update_score, json_return_value):
        mocked_request_data = {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91}
        scores_service_return = (
            True, {"away_score": 3, "home_score": 2, "match_id": 1, "match_length": 91, "score_id": 3})
        mocked_request.json.return_value = mocked_request_data
        update_score.return_value = scores_service_return
        json_return_value.return_value = scores_service_return[1]
        returned_value, response = api.update_score(3)

        assert update_score.called
        assert json_return_value.called
        self.assertEqual(scores_service_return[1], returned_value)
        self.assertEqual(200, response)

    @patch('src.services.scores_service.ScoresService.update_score')
    @patch('src.api.request')
    def test_update_score_valid_request_unsuccessful(self, mocked_request, update_Score):
        mocked_request_data = {}
        scores_service_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        update_Score.return_value = scores_service_return
        returned_value, response = api.update_score(1)

        assert update_Score.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_update_score_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.update_score(1)

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.services.scores_service.ScoresService.delete_score')
    def test_delete_score_successful(self, delete_score):
        score_id = 2
        expected_return_value = 'Score: ' + str(score_id) + ' deleted'
        delete_score.return_value = True
        returned_value, response = api.delete_score(score_id)

        assert delete_score.called
        self.assertEqual(expected_return_value, returned_value)
        self.assertEqual(202, response)

    @patch('src.services.scores_service.ScoresService.delete_score')
    def test_delete_score_unsuccessful(self, delete_score):
        delete_score.return_value = False
        returned_value, response = api.delete_score(0)

        assert delete_score.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.jsonify')
    @patch('src.services.scores_service.ScoresService.get_matches_without_scores')
    def test_get_matches_without_scores_successful(self, get_matches_without_scores, json_return_value):
        expected_json = {'matches': [{'A': 1}, {'A': 3}]}
        get_matches_without_scores.return_value = True, [{'A': 1}, {'A': 3}]
        json_return_value.return_value = expected_json
        returned_value, response = api.get_matches_without_scores()

        assert get_matches_without_scores.called
        assert json_return_value.called
        self.assertEqual(expected_json, returned_value)
        self.assertEqual(200, response)

    @patch('src.services.scores_service.ScoresService.get_matches_without_scores')
    def test_get_matches_without_scores_unsuccessful(self, get_matches_without_scores):
        get_matches_without_scores.return_value = False, []
        returned_value, response = api.get_matches_without_scores()

        assert get_matches_without_scores.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)


    #REFEREES

    def test_get_referees_successful(self):
        expected_json = {'referees': [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]}
        with patch('src.services.referees_service.RefereesService.get_referees') as get_referees:
            get_referees.return_value = True, [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]
            with patch('src.api.jsonify') as json_return_value:
                json_return_value.return_value = expected_json
                returned_value, response = api.get_referees()

                assert get_referees.called
                assert json_return_value.called
                self.assertEqual(expected_json, returned_value)
                self.assertEqual(200, response)

    def test_get_referees_unsuccessful(self):
        with patch('src.services.referees_service.RefereesService.get_referees') as get_referees:
            get_referees.return_value = False, []
            returned_value, response = api.get_referees()

            assert get_referees.called
            self.assertEqual('Something went wrong!', returned_value)
            self.assertEqual(500, response)


    @patch('src.api.jsonify')
    @patch('src.services.referees_service.RefereesService.add_referees')
    @patch('src.api.request')
    def test_add_referees_valid_request_successful(self, mocked_request, add_referees, json_return_value):
        mocked_request_data = {"name": "Harry Potter", "age": 52, "nationality": "British"}
        referees_service_return = (
            True, {"name": "Harry Potter", "age": 52, "nationality": "British", "referee_id": 3})
        mocked_request.json.return_value = mocked_request_data
        add_referees.return_value = referees_service_return
        json_return_value.return_value = referees_service_return[1]
        returned_value, response = api.add_referees()

        assert add_referees.called
        assert json_return_value.called
        self.assertEqual(referees_service_return[1], returned_value)
        self.assertEqual(201, response)

    @patch('src.services.referees_service.RefereesService.add_referees')
    @patch('src.api.request')
    def test_add_referees_valid_request_unsuccessful(self, mocked_request, add_referees):
        mocked_request_data = {}
        referees_service_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        add_referees.return_value = referees_service_return
        returned_value, response = api.add_referees()

        assert add_referees.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_add_referees_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.add_referees()

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.api.jsonify')
    @patch('src.services.referees_service.RefereesService.update_referee')
    @patch('src.api.request')
    def test_update_referee_valid_request_successful(self, mocked_request, update_referee, json_return_value):
        mocked_request_data = {"name": "Harry Potter", "age": 52, "nationality": "British"}
        referees_service_return = (
            True, {"name": "Harry Potter", "age": 52, "nationality": "British", "referee_id": 3})
        mocked_request.json.return_value = mocked_request_data
        update_referee.return_value = referees_service_return
        json_return_value.return_value = referees_service_return[1]
        returned_value, response = api.update_referee(3)

        assert update_referee.called
        assert json_return_value.called
        self.assertEqual(referees_service_return[1], returned_value)
        self.assertEqual(200, response)

    @patch('src.services.referees_service.RefereesService.update_referee')
    @patch('src.api.request')
    def test_update_referee_valid_request_unsuccessful(self, mocked_request, update_referee):
        mocked_request_data = {}
        referees_service_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        update_referee.return_value = referees_service_return
        returned_value, response = api.update_referee(1)

        assert update_referee.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_update_referee_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.update_referee(1)

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.api.jsonify')
    @patch('src.services.referees_service.RefereesService.get_referee')
    def test_get_referee_successful(self, get_referee, json_return_value):
        expected_json = ({"referees:":
            {
                "Name": "Harry Potter",
                "Age": 52,
                "Nationality": "British",
                "referee_id": 3
            }
        })
        get_referee.return_value = True, ({"referees:":
            {
                "Name": "Harry Potter",
                "Age": 52,
                "Nationality": "British",
                "referee_id": 3
            }
        })
        json_return_value.return_value = expected_json
        returned_value, response = api.get_referee(3)

        assert get_referee.called
        assert json_return_value.called
        self.assertEqual(expected_json, returned_value)
        self.assertEqual(200, response)

    @patch('src.services.referees_service.RefereesService.get_referee')
    def test_get_referee_unsuccessful(self, get_referee):
        get_referee.return_value = False, []
        returned_value, response = api.get_referee(0)

        assert get_referee.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)


    #END OF REFEREES


    @patch('src.api.jsonify')
    @patch('src.services.matches_service.MatchesService.get_matches')
    def test_get_matches_successful(self, get_matches, json_return_value):
        expected_json = ({"matches:":
            [
                {
                    "Stadium": "Etihad Stadium",
                    "away_team": "Shakhtar Donetsk",
                    "away_team_id": 2,
                    "date": "Fri, 30 Nov 2018 00:00:00 GMT",
                    "home_team": "Manchester City",
                    "home_team_id": 1,
                    "match_id": 1,
                    "referee": "Viktor Kassai",
                    "referee_id": 1,
                    "stadium_id": 1
                },
                {
                    "Stadium": "Etihad Stadium",
                    "away_team": "Shakhtar Donetsk",
                    "away_team_id": 2,
                    "date": "Mon, 19 Nov 2018 00:00:00 GMT",
                    "home_team": "Manchester City",
                    "home_team_id": 1,
                    "match_id": 4,
                    "referee": "Viktor Kassai",
                    "referee_id": 1,
                    "stadium_id": 1
                }
            ]
        })
        get_matches.return_value = True, ({"matches:":
            [
                {
                    "Stadium": "Etihad Stadium",
                    "away_team": "Shakhtar Donetsk",
                    "away_team_id": 2,
                    "date": "Fri, 30 Nov 2018 00:00:00 GMT",
                    "home_team": "Manchester City",
                    "home_team_id": 1,
                    "match_id": 1,
                    "referee": "Viktor Kassai",
                    "referee_id": 1,
                    "stadium_id": 1
                },
                {
                    "Stadium": "Etihad Stadium",
                    "away_team": "Shakhtar Donetsk",
                    "away_team_id": 2,
                    "date": "Mon, 19 Nov 2018 00:00:00 GMT",
                    "home_team": "Manchester City",
                    "home_team_id": 1,
                    "match_id": 4,
                    "referee": "Viktor Kassai",
                    "referee_id": 1,
                    "stadium_id": 1
                }
            ]
        })
        json_return_value.return_value = expected_json
        returned_value, response = api.get_matches()

        assert get_matches.called
        assert json_return_value.called
        self.assertEqual(expected_json, returned_value)
        self.assertEqual(200, response)

    @patch('src.services.matches_service.MatchesService.get_matches')
    def test_get_matches_unsuccessful(self, get_matches):
        get_matches.return_value = False, []
        returned_value, response = api.get_matches()

        assert get_matches.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.jsonify')
    @patch('src.services.matches_service.MatchesService.get_match')
    def test_get_match_successful(self, get_match, json_return_value):
        expected_json = ({"matches:":
            {
                "Stadium": "Etihad Stadium",
                "away_team": "Shakhtar Donetsk",
                "away_team_id": 2,
                "date": "Fri, 30 Nov 2018 00:00:00 GMT",
                "home_team": "Manchester City",
                "home_team_id": 1,
                "match_id": 1,
                "referee": "Viktor Kassai",
                "referee_id": 1,
                "stadium_id": 1
            }
        })
        get_match.return_value = True, ({"matches:":
            {
                "Stadium": "Etihad Stadium",
                "away_team": "Shakhtar Donetsk",
                "away_team_id": 2,
                "date": "Fri, 30 Nov 2018 00:00:00 GMT",
                "home_team": "Manchester City",
                "home_team_id": 1,
                "match_id": 1,
                "referee": "Viktor Kassai",
                "referee_id": 1,
                "stadium_id": 1
            }
        })
        json_return_value.return_value = expected_json
        returned_value, response = api.get_match(1)

        assert get_match.called
        assert json_return_value.called
        self.assertEqual(expected_json, returned_value)
        self.assertEqual(200, response)

    @patch('src.services.matches_service.MatchesService.get_match')
    def test_get_match_unsuccessful(self, get_match):
        get_match.return_value = False, []
        returned_value, response = api.get_match(0)

        assert get_match.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.jsonify')
    @patch('src.services.matches_service.MatchesService.add_match')
    @patch('src.api.request')
    def test_add_match_valid_request_successful(self, mocked_request, add_match, json_return_value):
        mocked_request_data = {"home_team_id":2, "away_team_id":1, "stadium_id":1, "referee_id":1, "date": "2018-11-30 00:00:00"}
        matches_service_return = True, {"home_team_id":2, "away_team_id":1, "stadium_id":1, "referee_id":1, "date": "2018-11-30 00:00:00"}
        mocked_request.json.return_value = mocked_request_data
        add_match.return_value = matches_service_return
        json_return_value.return_value = matches_service_return[1]
        returned_value, response = api.add_match()

        assert add_match.called
        assert json_return_value.called
        self.assertEqual(matches_service_return[1], returned_value)
        self.assertEqual(201, response)

    @patch('src.services.matches_service.MatchesService.add_match')
    @patch('src.api.request')
    def test_add_match_valid_request_unsuccessful(self, mocked_request, add_match):
        mocked_request_data = {}
        matches_service_return = (False, {})
        mocked_request.json.return_value = mocked_request_data
        add_match.return_value = matches_service_return
        returned_value, response = api.add_match()

        assert add_match.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_add_match_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.add_match()

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.api.jsonify')
    @patch('src.services.matches_service.MatchesService.update_match')
    @patch('src.api.request')
    def test_update_match_valid_request_successful(self, mocked_request, update_match, json_return_value):
        mocked_request_data = ({"away_team_id": 1,"date": "2018-11-30 00:00:00" , "home_team_id": 1, "match_id": 1,
                                "referee_id": 1, "stadium_id": 1})
        match_service_return = (
            True,  {"away_team_id": 1,"date": "2018-11-30 00:00:00" , "home_team_id": 1, "match_id": 1,
                    "referee_id": 1, "stadium_id": 1})
        mocked_request.json.return_value = mocked_request_data
        update_match.return_value = match_service_return
        json_return_value.return_value = match_service_return[1]
        returned_value, response = api.update_match(1)

        assert update_match.called
        assert json_return_value.called
        self.assertEqual(match_service_return[1], returned_value)
        self.assertEqual(200, response)

    @patch('src.services.matches_service.MatchesService.update_match')
    @patch('src.api.request')
    def test_update_match_valid_request_unsuccessful(self, mocked_request, update_match):
        mocked_request_data = {}
        match_service_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        update_match.return_value = match_service_return
        returned_value, response = api.update_match(1)

        assert update_match.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.services.matches_service.MatchesService.delete_match')
    def test_delete_match_successful(self, delete_match):
        match_id = 4
        expected_return_value = 'Match: ' + str(match_id) + ' deleted'
        delete_match.return_value = True
        returned_value, response = api.delete_match(match_id)

        assert delete_match.called
        self.assertEqual(expected_return_value, returned_value)
        self.assertEqual(202, response)

    @patch('src.services.matches_service.MatchesService.delete_match')
    def test_delete_match_unsuccessful(self, delete_match):
        delete_match.return_value = False
        returned_value, response = api.delete_match(0)

        assert delete_match.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.jsonify')
    @patch('src.services.stadiums_services.StadiumService.get_stadiums')
    def test_get_stadiums_successful(self, get_stadiums, json_return_value):
        expected_json = {'stadiums': [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]}
        get_stadiums.return_value = True, [{'A': 1, 'B': 2}, {'A': 3, 'B': 4}]
        json_return_value.return_value = expected_json
        returned_value, response = api.get_stadiums()

        assert get_stadiums.called
        assert json_return_value.called
        self.assertEqual(expected_json, returned_value)
        self.assertEqual(200, response)

    @patch('src.services.stadiums_services.StadiumService.get_stadiums')
    def test_get_stadiums_unsuccessful(self, get_stadiums):
        get_stadiums.return_value = False, []
        returned_value, response = api.get_stadiums()

        assert get_stadiums.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.jsonify')
    @patch('src.services.stadiums_services.StadiumService.add_stadium')
    @patch('src.api.request')
    def test_add_stadium_valid_request_successful(self, mocked_request, add_stadium, json_return_value):
        mocked_request_data = {"team_id": 1, "capacity": 2, "location": 2, "name": 1}
        stadiums_services_return = (
            True, {"team_id": 1, "capacity": 2, "location": 2, "name": 1, "stadium_id": 1})
        mocked_request.json.return_value = mocked_request_data
        add_stadium.return_value = stadiums_services_return
        json_return_value.return_value = stadiums_services_return[1]
        returned_value, response = api.add_stadium()

        assert add_stadium.called
        assert json_return_value.called
        self.assertEqual(stadiums_services_return[1], returned_value)
        self.assertEqual(201, response)

    @patch('src.services.stadiums_services.StadiumService.add_stadium')
    @patch('src.api.request')
    def test_add_stadium_valid_request_unsuccessful(self, mocked_request, add_stadium):
        mocked_request_data = {}
        stadiums_services_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        add_stadium.return_value = stadiums_services_return
        returned_value, response = api.add_stadium()

        assert add_stadium.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_add_stadium_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.add_stadium()

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.api.jsonify')
    @patch('src.services.stadiums_services.StadiumService.update_stadium')
    @patch('src.api.request')
    def test_update_stadium_valid_request_successful(self, mocked_request, update_stadium, json_return_value):
        mocked_request_data = {"team-id": 3, "capacity": 2, "location": 1, "name": 91}
        stadiums_services_return = (
            True, {"team_id": 3, "capacity": 2, "location": 1, "name": 91, "stadium_id": 3})
        mocked_request.json.return_value = mocked_request_data
        update_stadium.return_value = stadiums_services_return
        json_return_value.return_value = stadiums_services_return[1]
        returned_value, response = api.update_stadium(3)

        assert update_stadium.called
        assert json_return_value.called
        self.assertEqual(stadiums_services_return[1], returned_value)
        self.assertEqual(200, response)

    @patch('src.services.stadiums_services.StadiumService.update_stadium')
    @patch('src.api.request')
    def test_update_stadium_valid_request_unsuccessful(self, mocked_request, update_stadium):
        mocked_request_data = {}
        stadiums_services_return = (
            False, {})
        mocked_request.json.return_value = mocked_request_data
        update_stadium.return_value = stadiums_services_return
        returned_value, response = api.update_stadium(1)

        assert update_stadium.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)

    @patch('src.api.request')
    def test_update_stadium_invalid_request(self, mocked_request):
        mocked_request.json = None
        with self.assertRaises(HTTPException) as exc:
            api.update_stadium(1)

        self.assertTrue('400 Bad Request' in str(exc.exception))

    @patch('src.services.stadiums_services.StadiumService.delete_stadium')
    def test_delete_stadium_successful(self, delete_stadium):
        stadium_id = 3
        expected_return_value = 'stadium: ' + str(stadium_id) + ' deleted'
        delete_stadium.return_value = True
        returned_value, response = api.delete_stadium(stadium_id)

        assert delete_stadium.called
        self.assertEqual(expected_return_value, returned_value)
        self.assertEqual(202, response)

    @patch('src.services.stadiums_services.StadiumService.delete_stadium')
    def test_delete_stadium_unsuccessful(self, delete_stadium):
        delete_stadium.return_value = False
        returned_value, response = api.delete_stadium(0)

        assert delete_stadium.called
        self.assertEqual('Something went wrong!', returned_value)
        self.assertEqual(500, response)


if __name__ == '__main__':
    unittest.main()
