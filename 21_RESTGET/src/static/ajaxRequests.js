// File containing all jQuery ajax requests

// ajax request to get scores, callback is a function to perform when request completed
function ajaxGetScores(callback) {
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: "api/v1/scores",
        success: function (resp) {
            callback(resp);
        },
        error: function () {
            callback(null)
        }
    })
}

// ajax request to add a score, callback is a function to perform when request completed
function ajaxAddScore(callback, newScore) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "api/v1/scores",
        data: JSON.stringify(newScore),
        success: function (resp) {
            callback(resp);
        },
        error: function () {
            callback(null)
        }
    })
}

// ajax request to update a score, callback is a function to perform when request completed
function ajaxUpdateScore(callback, updatedScore, scoreId) {
    $.ajax({
        type: 'PUT',
        contentType: "application/json; charset=utf-8",
        url: "api/v1/scores/" + scoreId,
        data: JSON.stringify(updatedScore),
        success: function (resp) {
            callback(resp);
        },
        error: function () {
            callback(null)
        }
    })
}

// ajax request to delete a score, callback is a function to perform when request completed
function ajaxDeleteScore(callback, scoreId) {
    $.ajax({
        type: 'DELETE',
        url: "api/v1/scores/" + scoreId,
        success: function () {
            callback(true);
        },
        error: function () {
            callback(false)
        }
    })
}

// ajax request to get all matches without scores, callback is a function to perform when request completed
function ajaxGetMatchesWithoutScores(callback) {
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: "api/v1/scores/noScores",
        success: function (resp) {
            callback(resp);
        },
        error: function () {
            callback(null)
        }
    })
}
