var app = angular.module('app', []);

app.controller('AppCtrl', function AppCtrl($scope) {

    $scope.homeVisible = true;
    $scope.teamsVisible = false;
    $scope.playersVisible = false;
    $scope.matchesVisible = false;
    $scope.scoresVisible = false;
    $scope.managersVisible = false;
    $scope.stadiumsVisible = false;
    $scope.refereesVisible = false;

    // Shows and hides panels for different sections of the webapp
    $scope.showPanel = function (panelName) {
        // Hide all panels
        $scope.homeVisible = false;
        $scope.teamsVisible = false;
        $scope.playersVisible = false;
        $scope.matchesVisible = false;
        $scope.scoresVisible = false;
        $scope.managersVisible = false;
        $scope.stadiumsVisible = false;
        $scope.refereesVisible = false;

        // Show only the wanted panel
        switch (panelName) {
            case 'home':
                $scope.homeVisible = true;
                break;
            case 'teams':
                $scope.teamsVisible = true;
                break;
            case 'players':
                $scope.playersVisible = true;
                break;
            case 'matches':
                $scope.MmatchesVisible = true;
                break;
            case 'scores':
                $scope.scoresVisible = true;
                break;
            case 'managers':
                $scope.managersVisible = true;
                break;
            case 'stadiums':
                $scope.stadiumsVisible = true;
                break;
            case 'referees':
                $scope.refereesVisible = true;
                break;
            default:
                $scope.homeVisible = true;
        }
    }
});

// Set scope access modifiers to [[]] since Flask uses the default {{}}
app.config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
