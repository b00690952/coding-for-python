angular.module('app').component('scoresPanel', {
    templateUrl: "scoresPanel.html",
    controller: ScoresPanelController
});

function ScoresPanelController($scope) {

    // Scores panel variables
    $scope.scores = [];
    $scope.matchesWithoutScores = [];
    $scope.alertVisible = false;
    $scope.alertText = '';
    $scope.newScore = {
        'match_id': '',
        'home_score': '',
        'away_score': '',
        'match_length': ''
    };

    // Get scores and update the scope
    $scope.getScores = function () {
        ajaxGetScores(function (response) {
            if (response != null) {
                $scope.scores = responseToScores(response);
            } else {
                $scope.showAlert('Something went wrong getting scores')
            }
            $scope.$apply()
        })
    };

    // Get match ids without scores and update the scope
    $scope.getMatchesWithoutScores = function () {
        ajaxGetMatchesWithoutScores(function (response) {
            if (response != null) {
                var tempMatchesWithoutScores = [];
                response['matches'].forEach(function (m) {
                    tempMatchesWithoutScores.push(m['match_id']);
                });
                $scope.matchesWithoutScores = tempMatchesWithoutScores;
            } else {
                $scope.showAlert('Something went wrong getting matches without scores')
            }
            $scope.$apply()
        })
    };

    // Add a score to the database and update the scope
    $scope.addScore = function () {
        // Convert inputs to ints
        $scope.newScore.match_id = parseInt($scope.newScore.match_id, 10);
        $scope.newScore.home_score = parseInt($scope.newScore.home_score, 10);
        $scope.newScore.away_score = parseInt($scope.newScore.away_score, 10);
        $scope.newScore.match_length = parseInt($scope.newScore.match_length, 10);

        // Check all inputs are ints
        if (!isNaN($scope.newScore.match_id)
            && !isNaN($scope.newScore.home_score)
            && !isNaN($scope.newScore.away_score)
            && !isNaN($scope.newScore.match_length)) {

            // Add new score to database, and update the scope
            ajaxAddScore(function (response) {
                if (response != null) {
                    $scope.scores.push(responseToScore(response));
                    $scope.getMatchesWithoutScores()
                } else {
                    $scope.showAlert('Something went wrong adding values')
                }
                // Reset values in scope after ajax request completed
                $scope.newScore.match_id = '';
                $scope.newScore.home_score = '';
                $scope.newScore.away_score = '';
                $scope.newScore.match_length = '';
                $scope.$apply()
            }, $scope.newScore);
        } else {
            // If match_id is NaN set to '' so the correct option is selected
            if (isNaN($scope.newScore.match_id)) {
                $scope.newScore.match_id = '';
            }
            $scope.showAlert('Please enter valid values')
        }
    };

    // Flags the front end to make score editable
    $scope.editScore = function (score) {
        score.editing = true;
    };

    // Update a score in the database and update the scope
    $scope.updateScore = function (score) {
        // Convert inputs to ints
        var matchId = parseInt(score.matchId, 10);
        var homeScore = parseInt(score.homeScore, 10);
        var awayScore = parseInt(score.awayScore, 10);
        var matchLength = parseInt(score.matchLength, 10);

        // Check all inputs are ints
        if (!isNaN(matchId) && !isNaN(homeScore) && !isNaN(awayScore) && !isNaN(matchLength)) {
            var updatedScore = {
                "match_id": matchId,
                "home_score": homeScore,
                "away_score": awayScore,
                "match_length": matchLength
            };

            // Update score in database, and update the scope
            ajaxUpdateScore(function (response) {
                if (response != null) {
                    // Update the local score with response if it exists, else reload data
                    var responseScore = responseToScore(response);
                    var scoreIndex = $scope.scores.findIndex(function (score) {
                        return score.scoreId === responseScore.scoreId
                    });
                    // If the score index was found update the score in the scores array
                    if (scoreIndex !== -1) {
                        $scope.scores[scoreIndex] = responseScore
                    } else {
                        $scope.reloadDataAndAlert('Something went wrong updating score: ' + score.scoreId);
                    }
                } else {
                    $scope.reloadDataAndAlert('Something went wrong updating score: ' + score.scoreId);
                }
                // Stop editing and update scope after ajax request
                score.editing = false;
                $scope.getMatchesWithoutScores();
                $scope.$apply()
            }, updatedScore, score.scoreId);
        } else {
            $scope.showAlert('Please enter valid values');
        }
    };

    // Delete a score and update the scope
    $scope.deleteScore = function (scoreId) {
        ajaxDeleteScore(function (success) {
            if (success) {
                // Update scope if successful
                $scope.getScores();
                $scope.getMatchesWithoutScores()
            } else {
                $scope.showAlert('Something went wrong deleting score: ' + scoreId)
            }
            $scope.$apply()
        }, scoreId)
    };

    // Shows alert with message
    $scope.showAlert = function (message) {
        $scope.alertVisible = true;
        $scope.alertText = message;
    };

    // Closes the alert
    $scope.closeAlert = function () {
        $scope.alertVisible = false;
        $scope.alertText = '';
    };

    // Reloads data and shows an alert
    $scope.reloadDataAndAlert = function (message) {
        $scope.getScores();
        $scope.showAlert(message)
    };

    // Builds tweet about the selected score and opens it in a new tab
    $scope.tweetScore = function (score) {
        var tweetText = '';
        // Build tweet text based on score
        if (score.homeScore === score.awayScore) {
            tweetText = 'text=' + score.homeTeam + '%20drew%20with%20' + score.awayTeam
                + '%20' + score.homeScore.toString() + '-' + score.awayScore.toString() + '!'
        } else if (score.homeScore > score.awayScore) {
            tweetText = 'text=' + score.homeTeam + '%20beat%20' + score.awayTeam
                + '%20' + score.homeScore.toString() + '-' + score.awayScore.toString() + '!'
        } else {
            tweetText = 'text=' + score.awayTeam + '%20beat%20' + score.homeTeam
                + '%20' + score.awayScore.toString() + '-' + score.homeScore.toString() + '!'
        }
        // Build hashtags
        var tweetHashtags = 'hashtags=' + score.homeTeam.replace(/\s/g, '')
            + ',' + score.awayTeam.replace(/\s/g, '') + ',' + 'championsLeague';
        window.open('https://twitter.com/intent/tweet?' + tweetText + '&' + tweetHashtags, '_blank')
    };

    // Get data for panel on load
    $scope.getMatchesWithoutScores();
    $scope.getScores();
}

// Convert a response to an array of scores
function responseToScores(response) {
    var tempScores = [];
    response['scores'].forEach(function (s) {
        tempScores.push(responseToScore(s))
    });
    return tempScores
}

// Convert a response to a score
function responseToScore(response) {
    return new Score(
        response['score_id'],
        response['match_id'],
        response['away_score'],
        response['away_team'],
        response['home_score'],
        response['home_team'],
        response['match_length'],
        response['date'],
        response['referee_name'],
        response['stadium_name'])
}

// A score object
function Score(scoreId, matchId, awayScore, awayTeam, homeScore, homeTeam, matchLength, date, refereeName, stadiumName) {
    this.scoreId = scoreId;
    this.matchId = matchId;
    this.awayScore = awayScore;
    this.awayTeam = awayTeam;
    this.homeScore = homeScore;
    this.homeTeam = homeTeam;
    this.matchLength = matchLength;
    this.date = date;
    this.refereeName = refereeName;
    this.stadiumName = stadiumName;
    // Used to assist with editing of the score
    this.originalMatchId = matchId;
    this.editing = false
}
