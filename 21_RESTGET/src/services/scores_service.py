import mysql.connector


class ScoresService:
    __GET_SCORES_SQL = (
        "SELECT s.score_id, s.match_id, t1.name as home_team, "
        "s.home_score, t2.name as away_team, s.away_score, "
        "st.name as stadium_name, r.name as referee_name, s.match_length, m.date "
        "FROM scores s, matches m "
        "INNER JOIN teams t1 ON m.home_team_id = t1.team_id "
        "INNER JOIN teams t2 ON m.away_team_id = t2.team_id "
        "INNER JOIN stadiums st ON m.stadium_id = st.stadium_id "
        "INNER JOIN referees r ON m.referee_id = r.referee_id "
        "WHERE m.match_id = s.match_id;"
    )
    __GET_SCORE_SQL = (
        "SELECT s.score_id, s.match_id, t1.name as home_team, "
        "s.home_score, t2.name as away_team, s.away_score, "
        "st.name as stadium_name, r.name as referee_name, s.match_length, m.date "
        "FROM scores s, matches m "
        "INNER JOIN teams t1 ON m.home_team_id = t1.team_id "
        "INNER JOIN teams t2 ON m.away_team_id = t2.team_id "
        "INNER JOIN stadiums st ON m.stadium_id = st.stadium_id "
        "INNER JOIN referees r ON m.referee_id = r.referee_id "
        "WHERE m.match_id = s.match_id "
        "AND s.score_id ="
    )
    __ADD_SCORE_SQL = (
        "INSERT INTO scores(match_id, home_score, away_score, match_length) "
        "VALUES(%s, %s, %s, %s)"
    )
    __UPDATE_SCORE_SQL = (
        "UPDATE scores SET match_id = %s, home_score = %s, away_score = %s, match_length = %s WHERE score_id="
    )
    __DELETE_SCORE_SQL = (
        "DELETE FROM scores WHERE score_id="
    )
    __GET_MATCHES_WITHOUT_SCORES_SQL = (
        "SELECT m.match_id "
        "FROM matches m "
        "WHERE NOT EXISTS ("
        "   SELECT 1 "
        "   FROM scores s "
        "   WHERE m.match_id = s.match_id"
        ")"
    )

    def __init__(self, user, password, host, database):
        self.__user = user
        self.__password = password
        self.__host = host
        self.__database = database

    def get_scores(self):
        """
        Queries the database for all scores from the scores table and returns all of the
        rows in the table and relevant data from foreign keys including:
        away_score, away_team, date, home_score, home_team, match_id, referee_name, score_id, stadium_name

        :return: Success flag, All rows and related information from the scores table
        :rtype: (bool, list of dict)
        """
        result = []
        success = False

        try:
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__GET_SCORES_SQL)
            rows = cursor.fetchall()
            columns = cursor.description
            connection.close()
            for row in rows:
                result.append(self.__build_result(columns, row))
            success = True
        except Exception as ex:
            print('Something went wrong when getting scores: {}'.format(ex))

        return success, result

    def add_score(self, json):
        """
        Adds new score to the database, and returns it if successful

        :param json: json object from post request
        :type json: dict
        :return: Success flag, New row added to scores table
        :rtype: (bool, dict)
        """
        result = {}
        success = False

        try:
            if not self.__validate_input_json(json):
                raise TypeError('Input data not valid')
            # add new row
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__ADD_SCORE_SQL, (
                json['match_id'],
                json['home_score'],
                json['away_score'],
                json['match_length']
            ))
            new_id = cursor.lastrowid
            connection.commit()

            # get new row
            row, columns = self.__get_score(cursor, new_id)
            connection.close()

            result = self.__build_result(columns, row)
            success = True
        except Exception as ex:
            print('Something went wrong when adding score: {}'.format(ex))

        return success, result

    def update_score(self, json, score_id):
        """
        Update score from the database, and returns if it was successful

        :param json: json object from post request
        :type json: dict
        :param score_id: id of the score to be updated
        :type score_id: int
        :return: Success flag, New row added to scores table
        :rtype: (bool, dict)
        """
        result = {}
        success = False

        try:
            if not self.__validate_input_json(json):
                raise TypeError('Input data not valid')
            # add new row
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__UPDATE_SCORE_SQL + str(score_id), (
                json['match_id'],
                json['home_score'],
                json['away_score'],
                json['match_length'],
            ))
            connection.commit()

            # get updated row
            row, columns = self.__get_score(cursor, score_id)
            connection.close()
            if row is None:
                raise TypeError('Update performed on invalid score_id, row doesn\'t exist')

            result = self.__build_result(columns, row)
            success = True
        except Exception as ex:
            print('Something went wrong when updating score: {}'.format(ex))

        return success, result

    def delete_score(self, score_id):
        """
        Update score from the database, and returns if it was successful

        :param score_id: id of the score to be deleted
        :type score_id: int
        :return: Success flag
        :rtype: bool
        """
        success = False

        try:
            # Check if score exists
            connection = self.__get_connection()
            cursor = connection.cursor()
            row, columns = self.__get_score(cursor, score_id)
            if row is None:
                raise TypeError('Delete performed on invalid score_id, row doesn\'t exist')

            # Delete score
            cursor.execute(self.__DELETE_SCORE_SQL + str(score_id))
            connection.commit()
            connection.close()
            success = True
        except Exception as ex:
            print('Something went wrong when deleting score: {}'.format(ex))

        return success

    def get_matches_without_scores(self):
        """
        Queries the database for all matches that don't have a related score and returns the match_ids

        :return: Success flag, All rows and related information from the scores table
        :rtype: (bool, list of dict)
        """
        result = []
        success = False

        try:
            connection = self.__get_connection()
            cursor = connection.cursor()
            cursor.execute(self.__GET_MATCHES_WITHOUT_SCORES_SQL)
            rows = cursor.fetchall()
            columns = cursor.description
            connection.close()
            for row in rows:
                result.append(self.__build_result(columns, row))
            success = True
        except Exception as ex:
            print('Something went wrong when getting matches without scores: {}'.format(ex))

        return success, result

    def __get_connection(self):
        # Returns a connection to the database
        return mysql.connector.connect(
            user=self.__user,
            password=self.__password,
            host=self.__host,
            database=self.__database)

    def __get_score(self, cursor, score_id):
        # Gets an individual row, and returns the row and columns
        sql = self.__GET_SCORE_SQL + str(score_id)
        cursor.execute(sql)
        row = cursor.fetchone()
        columns = cursor.description
        return row, columns

    def __validate_input_json(self, json):
        if (isinstance(json['match_id'], int)
                and isinstance(json['home_score'], int)
                and isinstance(json['away_score'], int)
                and isinstance(json['match_length'], int)):
            return True
        else:
            return False

    def __build_result(self, columns, row):
        # Creates dictionary from columns and rows
        row_dict = {}
        for (key, value) in zip(columns, row):
            row_dict[key[0]] = value
        return row_dict
