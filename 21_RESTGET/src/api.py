from flask import Flask, jsonify, request, abort, render_template

from src.services.stadiums_services import StadiumService
from src.services.matches_service import MatchesService
from src.services.referees_service import RefereesService
from src.services.scores_service import ScoresService

# URIs
VERSION = 'v1'
BASE_URI = '/api/' + VERSION

SCORES = '/scores'
SCORES_URI = BASE_URI + SCORES

STADIUMS = '/stadiums'
STADIUMS_URI = BASE_URI + STADIUMS

REFEREES = '/referees'
REFEREES_URI = BASE_URI + REFEREES

MATCHES = '/matches'
MATCHES_URI = BASE_URI + MATCHES

# Database info, update for your DB
USER = 'root'
PASSWORD = ''
HOST = 'localhost'
DATABASE = 'team_29_db'

# Services
scores_service = ScoresService(USER, PASSWORD, HOST, DATABASE)
stadiums_services = StadiumService(USER, PASSWORD, HOST, DATABASE)
referees_service = RefereesService(USER, PASSWORD, HOST, DATABASE)
matches_service = MatchesService(USER, PASSWORD, HOST, DATABASE)

app = Flask(__name__, )


# Templates
@app.route('/')
def index():
    return render_template('index.html')


@app.route('/homePanel.html')
def home_panel():
    return render_template('homePanel.html')


@app.route('/scoresPanel.html')
def scores_panel():
    return render_template('scoresPanel.html')


@app.route(BASE_URI)
def api_base_uri():
    return 'Team 29 API'


# Scores
@app.route(SCORES_URI, methods=['GET'])
def get_scores():
    success, rows = scores_service.get_scores()
    if success:
        return jsonify({'scores': rows}), 200
    else:
        return 'Something went wrong!', 500


@app.route(SCORES_URI, methods=['POST'])
def add_score():
    if not request.json:
        abort(400)

    success, row = scores_service.add_score(request.json)
    if success:
        return jsonify(row), 201
    else:
        return 'Something went wrong!', 500


@app.route(SCORES_URI + '/<int:score_id>', methods=['PUT'])
def update_score(score_id):
    if not request.json:
        abort(400)

    success, row = scores_service.update_score(request.json, score_id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


@app.route(SCORES_URI + '/<int:score_id>', methods=['DELETE'])
def delete_score(score_id):
    success = scores_service.delete_score(score_id)
    if success:
        return 'Score: ' + str(score_id) + ' deleted', 202
    else:
        return 'Something went wrong!', 500


@app.route(SCORES_URI + '/noScores', methods=['GET'])
def get_matches_without_scores():
    success, rows = scores_service.get_matches_without_scores()
    if success:
        return jsonify({'matches': rows}), 200
    else:
        return 'Something went wrong!', 500


# Stadiums
@app.route(STADIUMS_URI, methods=['GET'])
def get_stadiums():
    success, rows = stadiums_services.get_stadiums()
    if success:
        return jsonify({'stadiums': rows}), 200
    else:
        return 'Something went wrong!', 500


@app.route(STADIUMS_URI + '/<int:id>', methods=['GET'])
def get_stadium(stadium_id):
    success, row = stadiums_services.get_stadium(stadium_id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


@app.route(STADIUMS_URI, methods=['POST'])
def add_stadium():
    if not request.json:
        abort(400)

    success, row = stadiums_services.add_stadium(request.json)
    if success:
        return jsonify(row), 201
    else:
        return 'Something went wrong!', 500


@app.route(STADIUMS_URI + '/<int:id>', methods=['PUT'])
def update_stadium(stadium_id):
    if not request.json:
        abort(400)

    success, row = stadiums_services.update_stadium(request.json, stadium_id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


@app.route(STADIUMS_URI + '/<int:id>', methods=['DELETE'])
def delete_stadium(stadium_id):
    success = stadiums_services.delete_stadium(stadium_id)
    if success:
        return 'stadium: ' + str(stadium_id) + ' deleted', 202
    else:
        return 'Something went wrong!', 500


# Matches
@app.route(MATCHES_URI, methods=['GET'])
def get_matches():
    success, rows = matches_service.get_matches()
    if success:
        return jsonify({'matches': rows}), 200
    else:
        return 'Something went wrong!', 500


@app.route(MATCHES_URI + '/<int:id>', methods=['GET'])
def get_match(id):
    success, row = matches_service.get_match(id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


@app.route(MATCHES_URI, methods=['POST'])
def add_match():
    if not request.json:
        abort(400)

    success, row = matches_service.add_match(request.json)
    if success:
        return jsonify(row), 201
    else:
        return 'Something went wrong!', 500


@app.route(MATCHES_URI + '/<int:id>', methods=['PUT'])
def update_match(id):
    if not request.json:
        abort(400)

    success, row = matches_service.update_match(request.json, id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


@app.route(MATCHES_URI + '/<int:id>', methods=['DELETE'])
def delete_match(id):
    success = matches_service.delete_match(id)
    if success:
        return 'Match: ' + str(id) + ' deleted', 202
    else:
        return 'Something went wrong!', 500


# Referees
@app.route(REFEREES_URI, methods=['GET'])
def get_referees():
    success, rows = referees_service.get_referees()
    if success:
        return jsonify({'referees': rows}), 200
    else:
        return 'Something went wrong!', 500


@app.route(REFEREES_URI, methods=['POST'])
def add_referees():
    if not request.json:
        abort(400)

    success, row = referees_service.add_referees(request.json)
    if success:
        return jsonify(row), 201
    else:
        return 'Something went wrong!', 500


@app.route(REFEREES_URI + '/<int:referee_id>', methods=['PUT'])
def update_referee(referee_id):
    if not request.json:
        abort(400)

    success, row = referees_service.update_referee(request.json, referee_id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


@app.route(REFEREES_URI + '/<int:id>', methods=['GET'])
def get_referee(id):
    success, row = referees_service.get_referee(id)
    if success:
        return jsonify(row), 200
    else:
        return 'Something went wrong!', 500


# end of referees


if __name__ == '__main__':
    app.run(debug=True)
